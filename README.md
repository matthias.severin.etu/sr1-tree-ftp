# Tree FTP - Matthias SEVERIN

## Generation de la javadoc

mvn javadoc:javadoc

## Generation du fichier jar

mvn package

## Execution avec 1 parametre

java -jar target\TREE_FTP-1.0-SNAPSHOT.jar [IP du serveur]

Permet de lancer la methode tree sur le serveur passe en parametre

Exemple : java -jar target\TREE_FTP-1.0-SNAPSHOT.jar ftp.ubuntu.com

## Execution avec 2 parametres

java -jar target\TREE_FTP-1.0-SNAPSHOT.jar [IP du serveur] [profondeur]

Permet de lancer la methode tree sur le serveur passe en parametre avec une profondeur maximale

Exemple : java -jar target\TREE_FTP-1.0-SNAPSHOT.jar ftp.ubuntu.com 3

## Execution avec 4 parametres

java -jar target\TREE_FTP-1.0-SNAPSHOT.jar [IP du serveur] [profondeur] [username] [password]

Permet de lancer la methode tree sur le serveur passe en parametre en se connectant avec son nom d'utilisateur et son mot de passe

Exemple sans profondeur maximale : java -jar target\TREE_FTP-1.0-SNAPSHOT.jar ftp.ubuntu.com -1 toto titi

Exemple avec profondeur maximale : java -jar target\TREE_FTP-1.0-SNAPSHOT.jar ftp.ubuntu.com 2 toto titi

## Remarque

Comme je vous l'ai dit par mail, il y a un probleme avec le dossier /pub/assistance qui fait crash la connexion au serveur FTP et je ne sais toujours pas pourquoi (Filezilla crash aussi sur ce dossier)
D'autre dossier font pareil sur ce serveur FTP : la semaine derniere le dossier /tmp ne fonctionnait pas et maintenant il fonctionne mais ce n'est plus le cas pour /stats