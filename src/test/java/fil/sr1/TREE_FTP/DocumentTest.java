package fil.sr1.TREE_FTP;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Tests de la classe Document
 * @author Matthias Severin
 */
public class DocumentTest {
	
    /**
     * Test de la generation d'un fichier executable
     */
    @Test
    public void testGetDocumentFromFileExecutable() {
        String data = "-rwxr-xr-x   31 997      997          4096 Feb 01 13:47 file";
        Document file = Document.getDocumentFromData(data);
        assertTrue(file.name.equals("file"));
        assertTrue(file.type == DocumentType.FILE);
        assertTrue(file.executable);
    }
    
    /**
     * Test de la generation d'un fichier non executable
     */
    @Test
    public void testGetDocumentFromFileNotExecutable() {
        String data = "-rwxr-xr--   31 997      997          4096 Feb 01 13:47 file";
        Document file = Document.getDocumentFromData(data);
        assertTrue(file.name.equals("file"));
        assertTrue(file.type == DocumentType.FILE);
        assertTrue(! file.executable);
    }
    
    /**
     * Test de la generation d'un dossier executable
     */
    @Test
    public void testGetDocumentFromDirectoryExecutable() {
        String data = "drwxr-xr-x   31 997      997          4096 Feb 01 13:47 directory";
        Document file = Document.getDocumentFromData(data);
        assertTrue(file.name.equals("directory"));
        assertTrue(file.type == DocumentType.DIRECTORY);
        assertTrue(file.executable);
    }
    
    /**
     * Test de la generation d'un dossier non executable
     */
    @Test
    public void testGetDocumentFromDirectoryNotExecutable() {
        String data = "drwxr-xr--   31 997      997          4096 Feb 01 13:47 directory";
        Document directory = Document.getDocumentFromData(data);
        assertTrue(directory.name.equals("directory"));
        assertTrue(directory.type == DocumentType.DIRECTORY);
        assertTrue(! directory.executable);
    }
    
    /**
     * Test de la generation d'un lien executable
     */
    @Test
    public void testGetDocumentFromLinkExecutable() {
        String data = "lrwxr-xr-x   31 997      997          4096 Feb 01 13:47 linkTo -> this";
        Document link = Document.getDocumentFromData(data);
        assertTrue(link.name.equals("linkTo -> this"));
        assertTrue(link.type == DocumentType.LINK);
        assertTrue(link.executable);
    }
    
    /**
     * Test de la generation d'un lien non executables
     */
    @Test
    public void testGetDocumentFromLinkNotExecutable() {
        String data = "lrwxr-xr--   31 997      997          4096 Feb 01 13:47 linkTo -> this";
        Document link = Document.getDocumentFromData(data);
        assertTrue(link.name.equals("linkTo -> this"));
        assertTrue(link.type == DocumentType.LINK);
        assertTrue(! link.executable);
    }
}