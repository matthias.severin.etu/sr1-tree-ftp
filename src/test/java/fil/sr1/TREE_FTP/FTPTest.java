package fil.sr1.TREE_FTP;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Tests de la classe FTP
 * @author Matthias Severin
 */
public class FTPTest {
	
    /**
     * Test de getIpFromData
     */
    @Test
    public void testGetIpFromData() {
        String data = "227 Entering Passive Mode (91,189,88,142,36,60).";
        String ip = FTP.getIpFromData(data);
        assertTrue(ip.equals("91.189.88.142"));
        
        data = "227 Entering Passive Mode (84,179,92,132,23,51).";
        ip = FTP.getIpFromData(data);
        assertTrue(ip.equals("84.179.92.132"));
    }
    
    /**
     * Test de getPortFromData
     */
    @Test
    public void testGetPortFromData() {
        String data = "227 Entering Passive Mode (91,189,88,142,36,60).";
        int port = FTP.getPortFromData(data);
        assertTrue(port == 9276);
        
        data = "227 Entering Passive Mode (84,179,92,132,23,51).";
        port = FTP.getPortFromData(data);
        assertTrue(port == 5939);
    }
}