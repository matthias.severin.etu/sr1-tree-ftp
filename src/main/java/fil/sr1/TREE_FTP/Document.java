package fil.sr1.TREE_FTP;

/**
 * Classe qui decrit les documents
 * @author Matthias Severin
 */
public class Document {
	
	protected String name;
	protected DocumentType type;
	protected boolean executable;
	
	/**
	 * @param name le nom du fichier
	 * @param type le type du fichier
	 * @param executable un boolean qui montre si le fichier est executable ou non
	 */
	public Document(String name, DocumentType type, boolean executable) {
		this.name = name;
		this.type = type;
		this.executable = executable;
	}
	
	/**
	 * @param data les informations d'un fichier
	 * @return genere le Document qui correspond a data
	 */
	public static Document getDocumentFromData(String data) {
		DocumentType type;
		switch (data.charAt(0)) {
		case ('d') :
			type = DocumentType.DIRECTORY;
			break;
		case ('l') :
			type = DocumentType.LINK;
			break;
		default :
			type = DocumentType.FILE;
		}
		String[] dataArray = data.split(" ");
		String name = "";
		if (type == DocumentType.LINK) {
			name = dataArray[dataArray.length - 3] + " -> ";
		}
		name += dataArray[dataArray.length - 1];
		return new Document(name , type, data.charAt(9) == 'x');
	}
	
	public String toString() {
		return this.name;
	}
}
