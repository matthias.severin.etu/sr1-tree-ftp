package fil.sr1.TREE_FTP;

/**
 * Enumeration des differents type de document possible
 * @author Matthias Severin
 */
public enum DocumentType {
	FILE, DIRECTORY, LINK;
}
