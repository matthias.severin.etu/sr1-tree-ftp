package fil.sr1.TREE_FTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Classe qui s'occupe de la gestion des serveurs FTP
 * @author Matthias Severin
 */
public class FTP {
	
	private Socket socket;
	private PrintWriter printer;
	private BufferedReader reader;
	private boolean debug;
	
	/**
	 * @param ip l'ip du serveur
	 * @param port le port sur lequel il faut se connecter
	 * @param debug si debug est a true les messages envoyes par le serveur seront affiches, sinon ils ne le seront pas
	 */
	public FTP(String ip, int port, boolean debug) {
		this.debug = debug;
		try {
			socket = new Socket(ip , port);
			
			OutputStream out = socket.getOutputStream();
			printer = new PrintWriter(out, true);
			
			InputStream in = socket.getInputStream();
			InputStreamReader isr= new InputStreamReader(in);
			reader = new BufferedReader(isr);
		} catch (IOException e) {
			throw new FTPException("Cannot open the connexion with the server");
		}
	}
	
	/**
	 * Envoi la commande command au serveur FTP
	 * @param command une commande FTP
	 */
	public void write(String command) {
		printer.println(command);
	}
	
	/**
	 * Recupere un message envoye par le serveur
	 * @return le message recu
	 */
	public String readLine() {
		try {
			String res = reader.readLine();
			if (debug) {
				System.out.println(res);
			}
			return res;
		} catch (IOException e) {
			throw new FTPException("Cannot read the message");
		}
	}
	
	/**
	 * Cree la connexion avec le serveur FTP en mode anonyme
	 */
	public void connect() {
		connect("anonymous", "anonymous");
	}
	
	/**
	 * Cree la connexion avec le serveur FTP
	 * @param username le nom de l'utilisateur
	 * @param password le mot de passe
	 */
	public void connect(String username, String password) {
		readLine();
		
		write("AUTH TLS");
		readLine();
		
		write("AUTH SSL");
		readLine();
		
		write("USER " + username);
		readLine();
		
		write("PASS " + password);
		readLine();
		
		write("PWD");
		readLine();
		
		write("TYPE I");
		readLine();
	}
	
	/**
	 * Ferme la connexion avec le serveur
	 */
	public void close() {
		try {
			socket.close();
		} catch (IOException e) {
			throw new FTPException("Cannot close the socket");
		}
	}
	
	/**
	 * Parse le message data pour retourner l'ip du nouveau socket
	 * @param data le message recu par le serveur qui donne les informations de l'ip/port d'un nouveau socket
	 * @return l'adresse ip du nouveau socket
	 */
	public static String getIpFromData(String data) {
		String dataArray[] = data.substring(data.indexOf('(') + 1, data.indexOf(')')).split(",");
		String ip = dataArray[0];
		for (int i = 1; i < 4; i++) {
			ip = ip + "." + dataArray[i];
		}
		return ip;
	}
	
	/**
	 * Parse le message data pour retourner le port du nouveau socket
	 * @param data le message recu par le serveur qui donne les informations de l'ip/port d'un nouveau socket
	 * @return le port du nouveau socket
	 */
	public static int getPortFromData(String data) {
		String dataArray[] = data.substring(data.indexOf('(') + 1, data.indexOf(')')).split(",");
		int port = (Integer.parseInt(dataArray[4]) * 256) + Integer.parseInt(dataArray[5]);
		return port;
	}
	
	/** Envoi la commande PASV au serveur FTP et se connecte au nouveau socket
	 * @return la connexion au nouveau socket
	 */
	public FTP pasv() {
		write("PASV");
		String result = readLine();
		String ip = getIpFromData(result);
		int port = getPortFromData(result);
		return new FTP(ip, port, false);
	}
	
	/**
	 * Liste le contenu du repertoire actuel et ferme la connexion a receiver
	 * @param receiver une connexion FTP qui permet de recevoir des informations demandees par le 
	 * serveur FTP principal (obtenu via la commande pasv)
	 * @return la liste des documents contenus dans le repertoire actuelle
	 */
	public ArrayList<Document> list(FTP receiver) {
		ArrayList<Document> content = new ArrayList<>();
		write("LIST");
		readLine();
		String fileData = receiver.readLine();
		while (fileData != null) {
			content.add(Document.getDocumentFromData(fileData));
			fileData = receiver.readLine();
		}
		receiver.close();
		readLine();
		return content;
	}
	
	/**
	 * Affiche le contenu du repertoire actuel en combinant les commandes pasv et list
	 */
	public void listCurrentDirectory() {
		FTP receiver = pasv();
		ArrayList<Document> content = list(receiver);
		for (Document document : content) {
			System.out.println(document);
		}
	}
	
	/** Se deplace dans le repertoire directory
	 * @param directory un repertoire
	 */
	public void cwd(String directory) {
		write("CWD " + directory);
		readLine();
	}
	
	/**
	 * Retourne dans le repertoire parent
	 */
	public void cdup() {
		write("CDUP");
		readLine();
	}
	
	/**
	 * Affiche l'arborescense du repertoire actuel
	 * @param indent permet de definir l'indentation initiale
	 * @param depth profondeur maximale
	 */
	private void tree(int indent, int depth) {
		FTP receiver = pasv();
		ArrayList<Document> content = list(receiver);
		for (Document document : content) {
			for (int i = 0; i < indent; i++) {
				System.out.print("\t");
			}
			System.out.println(document.name);
			if (document.type == DocumentType.DIRECTORY) {
				if (document.executable) {
					if (depth == -1 || indent < depth - 1) {
						this.cwd(document.name);
						tree(indent + 1, depth);
						this.cdup();
					}
				}
			}
		}
	}
	
	/**
	 * Affiche l'arborescense du repertoire actuel sans profondeur maximale
	 */
	public void tree() {
		tree(0, -1);
	}
	
	/**
	 * Affiche l'arborescense du repertoire actuel avec une profondeur maximale
	 * @param depth la profondeur maximale
	 */
	public void tree(int depth) {
		tree(0, depth);
	}

}
