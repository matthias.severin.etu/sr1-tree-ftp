package fil.sr1.TREE_FTP;

/**
 * Exception declenchee pour les erreurs avec les serveurs FTP
 * @author Matthias Severin
 */
public class FTPException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public FTPException(String msg) {
		super(msg);
	}

}
