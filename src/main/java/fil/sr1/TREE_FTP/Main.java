package fil.sr1.TREE_FTP;

/**
 * Classe principal du projet de SR1 : TREE FTP
 * @author Matthias Severin
 */
public class Main {

	public static void main(String[] args) {
		FTP ftp;
		
		if (args.length > 0) {
			ftp = new FTP(args[0], 21, false);
			
			if (args.length > 3) {
				ftp.connect(args[2], args[3]);
			} else {
				ftp.connect();
			}
			
			if (args.length > 1) {
				ftp.tree(Integer.parseInt(args[1]));
			} else {
				ftp.tree();
			}
			
			ftp.close();
			
		} else {
			System.out.println("You need at least 1 argument");
		}
	}
}
